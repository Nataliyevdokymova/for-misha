import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() =>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(backgroundColor: Colors.deepPurpleAccent),
        home: Scaffold(
          backgroundColor: Colors.deepPurpleAccent,
            body: Column (
              mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
              Text ("Ой!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.white,
                      fontFamily: 'Microsoft Sans Serif'
                  ),),
                Text ("Ваше інтернет з'єднання"
                    "відсутнє.\nБудь ласка підключіться до мережі, \n та"
                    " спробуйте знову",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontFamily: 'Microsoft Sans Serif'
                    ),),
                ]
            ),
          floatingActionButton: FloatingActionButton(
            child: Text('Спробувати знову',
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.deepPurpleAccent,
                  fontFamily: 'Microsoft Sans Serif'
              ),),
  backgroundColor: Colors.white,
  onPressed: (){
    print ('clicked');
  },
),
            ),
    );
  }
}


